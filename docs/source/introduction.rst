EnerGyan
========


EnerGyan Features
^^^^^^^^^^^^^^^^^

* Electrical Hardware - contains a grid in a box. Comes with complete energy hardware - energy sources, loads, converters, batteries, energy management boards (called *HEM*). 
* Computing - EnerGyan computing system can run your energy application. 
* Communication - EnerGyan comes with a number of communication capabilities - WiFi, Ethernet, GSM/GPRS/3G and Bluetooth. 
* Data storage - EnerGyan has on-board data storage which is continuously storing all electrical parameters. You can retrieve this data whenever you want. 
* Software - EnerGyan's software called *HEMApp* keeps the module up & running and does all the heavy lifting for you. 



Nomenclature 
^^^^^^^^^^^^

* **Loads** - an entity which consumes energy is called a load. This can be a TV or your phone. 
* **Source** - an entity which produces energy. This can be a solar panel, wind turbine etc. 
* **Storage** - an entity which can store energy. Most common is a battery and they come in different varieties - lead acid, Li ion etc. 
* **Nodes** - these are hardware ports that are used to physically connect a source or a load. It's like an electric plug point you use to connect your phone charger. Every EnerGyan module has a fixed number of ports. EnerGyan DC has 8 and EnerGyan AC has 6 ports. 
* **HEM** - Homegrid Energy Manager. This is the brain (energy management) of EnerGyan. It is custom built hardware and software which help manage EnerGyan.This keeps EnerGyan running and does all the heavy lifting. Every EnerGyan model has a HEM system.  



What does EnerGyan DC contain?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

It contains the following items 

* Solar panel
* One 7Ah battery for energy system
* One 7Ah battery for management system
* One HEM system (management system)
* Battery charger port
* Two battery chargers for the batteries 
* 12V to 5V USB charger
* 12V car charger
* Eight nodes (you can connect loads/sources)


What does EnerGyan AC contain?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

It contains the following items 

* Solar panel
* One 7Ah battery for energy system
* One 7Ah battery for management system
* One HEM system (management system)
* Battery charger port
* Two battery chargers for the batteries 
* Inverter (120V AC output)
* 6 nodes (only loads). 


