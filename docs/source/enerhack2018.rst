EnerHack 2018
=============


EnerGyan team assignment
------------------------

* Bears in Chairs ---EnerGyan DC5
* E -----------------EnerGyan AC2
* Solaris------------EnerGyan DC2
* FPGA---------------EnerGyan AC1
* becky Badgers------EnerGyan DC1
* Vidyut-------------EnerGyan DC4
* energySaver--------EnerGyan DC3



This section has resources for participants with the EnerHack Energy Hackathon 2018. 



EnerGyan Hardware
-----------------

DC1
^^^

IP - ``192.168.1.207``

* Node 0: Solar
* Node 1: Battery Charger
* Node 2: All Fans
* Node 3: Mini fridge
* Node 4: N/A
* Node 5: N/A
* Node 6: LED Lights
* Node 7: Radio


.. image:: images/dc1.jpg
	:scale: 15%


DC2
^^^

IP - ``192.168.1.159``

* Node 0: Solar
* Node 1: LED Light
* Node 2: Charger
* Node 3: LED Light Strip
* Node 4: N/A
* Node 5: N/A
* Node 6: DC/AC Converter (parallel with Node 7)
* Node 7: DC/AC Converter (parallel with Node 6)

.. image:: images/dc2.png
	:scale: 65%




DC3
^^^

IP - ``192.168.1.28``

* Node 0: Battery charger
* Node 1: Right fan bolted next to DC 3 sign
* Node 2: Jhua fan second lowest platform
* Node 3: White light


.. image:: images/dc3.png
	:scale: 45%




DC4
^^^

IP - ``192.168.1.152``

* Node 0: Battery charger
* Node 1: Left fan bolted next to the DC 4 sign
* Node 2: USB Led light string
* Node 3: Mitchell Fan


.. image:: images/dc4.png
	:scale: 45%




DC5
^^^

IP - ``192.168.1.177``

* Node 0: (top light)
* Node 1: (middle light)
* Node 2: (bottom light)
* Node 3: (dual fan)
* Node 4: N/A
* Node 5: Nothing plugged in to USB port
* Node 6: Nothing plugged in to car charger



.. image:: images/dc5.jpg
	:scale: 15%


DC6
^^^

IP - ``192.168.1.217``

* Node 0: Charger
* Node 1: Fan
* Node 2: Fan plugged in to car charger
* Node 3: LED light string

AC1
^^^

IP - ``192.168.1.100``

* Node 0: Fan
* Node 1: Desk Lamp
* Node 2: NA
* Node 3: NA

.. image:: images/ac1.png
	:scale: 65%




AC2
^^^

IP - ``192.168.1.236``


* Node 0: Fan
* Node 1: Desk Lamp
* Node 2: NA
* Node 3: NA



Other hardware
--------------



Misc
----
