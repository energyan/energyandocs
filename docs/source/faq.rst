Frequently Asked Questions
==========================

Where can EnerGyan be used?
^^^^^^^^^^^^^^^^^^^^^^^^^^^

EnerGyan is a flexible platform that can used by students at various levels. 
It is also a research platform for R&D in energy systems. 

* High school
* Technical colleges
* Undergraduate and graduate programs 
* Energy centers and companies for R&D


What can you learn/teach with EnerGyan?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Intro to energy systems
* Smart and micro grids
* IoT and smart homes
* Mobile and cloud computing for energy systems
* Optimization, AI and ML for energy systems


How do students program EnerGyan?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: images/program.png


What can EnerGyan do?
^^^^^^^^^^^^^^^^^^^^^

EnerGyan can function as the following. You can think of more ways to use EnerGyan.

* A microgrid
* A smart home 
* A solar charging station
* Bunch of EnerGyans can be networked together to form a larger microgrid
* IoT testbed


How can you teach with EnerGyan?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

EnerGyan can be used in a number of ways

* Semester long course 
* 2 week workshop on energy systems 
* 3 day hackathons

We've done all of these with EnerGyan. Its adaptable to your application. 


Who's the audience for this?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Anybody with a passion for energy and basic programming skills.


Is it just software-based education?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

No! EnerGyan is software and HARDWARE customizable. Here are some things that you can do (limited list)

* You can change the type of source or load. 
* You can change battery type or size.
* You can connect different type of sensors (moisture, temperature, light etc.)
* You can connect smart home devices with EnerGyan - Amazon Echo, Google Home, Wemo switches etc. 
* You can change the configuration of your energy system. You can turn your EnerGyan smart home to EnerGyan Community Solar charging station by simple re-wiring. 
* The list goes on ...
