EnerGyan
========

EnerGyan (pronounced /eːnər ɡiːˈɑːn/) is an energy education and research platform developed for high school, undergraduate and graduate students with focus on energy. It was designed and developed at UW-Madison. It also serves as a research platform for scientists, energy engineers and researchers pursing advanced studies in power system, electronics, IoT, cyber-physical systems, and energy system etc.

EnerGyan in one sentence
------------------------

It's a programmable electricity grid-in-a-box. 

EnerGyan in one picture
-----------------------

.. image:: images/energyan.png 
	:scale: 70%



Why EnerGyan?
-------------

.. raw:: html 

   <video controls src="_static/energyan_docs.mp4" width="560" height="315"></video> 


Contents:
^^^^^^^^^
.. toctree::
   :maxdepth: 3
    
   contact
   faq
   introduction
   devAppsHem
   troubleshoot
   enerhack2018


.. Indices and tables
.. ==================

.. * :ref:`search`

.. * :ref:`genindex`
.. * :ref:`modindex`
